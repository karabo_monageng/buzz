package com.android.buzz.features.weather

import com.squareup.moshi.*

@JsonClass(generateAdapter = true)
data class WeatherResponseModel(
    var id: Int? = 0,
    var name: String? = "",
    var weather: List<Weather>? = null,
    var main: Main? = null
)

@JsonClass(generateAdapter = true)
data class Weather (
    var id: Int? = 0,
    var main: String? = "",
    var description: String? = "",
    var icon: String? = ""
)

@JsonClass(generateAdapter = true)
data class Main (
    var temp: Float? = 0f,
    @Json(name = "feels_like")
    var feelsLike: Float? = 0f,
    var humidity: Int? = 0
)