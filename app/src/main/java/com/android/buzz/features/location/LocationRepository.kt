package com.android.buzz.features.location

import android.app.Application
import com.android.buzz.database.BuzzDatabase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.suspendCancellableCoroutine
import javax.inject.Inject
import kotlin.coroutines.resume

class LocationRepository @Inject constructor(application: Application) {
    private val buzzDb = BuzzDatabase.getDatabase(application)

    suspend fun insertLocation(locationModel: UserLocationModel) : Boolean = suspendCancellableCoroutine { continuation ->
        GlobalScope.launch(Dispatchers.IO) {
            with(buzzDb?.locationDao()) {
                this?.insertLocation(locationModel)
            }
        }
        continuation.resume(true)
    }

}