package com.android.buzz.features.location

import androidx.room.*

@Dao
interface LocationDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLocation(location: UserLocationModel)

    @Query("SELECT * FROM UserLocation")
    fun getLocationList() : List<UserLocationModel>

    @Delete
    fun deleteLocation(location: UserLocationModel)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    fun updateLocation(location: UserLocationModel)
}