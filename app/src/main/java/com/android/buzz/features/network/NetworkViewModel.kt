package com.android.buzz.features.network

import android.Manifest
import android.app.Activity
import android.content.Context.TELEPHONY_SERVICE
import android.content.Context.WIFI_SERVICE
import android.content.pm.PackageManager
import android.net.wifi.WifiManager
import android.os.Build
import android.telephony.*
import androidx.annotation.RequiresApi
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.buzz.utils.Constants
import org.json.JSONObject
import javax.inject.Inject

class NetworkViewModel @Inject constructor() : ViewModel() {
    val fetchCellInfoResponse = MutableLiveData<HashMap<Int, JSONObject>>()
    val fetchWifiInfoResponse = MutableLiveData<HashMap<String, JSONObject>>()

    @RequiresApi(Build.VERSION_CODES.Q)
    fun fetchCellInfo(activity: Activity) {
        val telephony = activity.getSystemService(TELEPHONY_SERVICE) as TelephonyManager
        val phoneType = telephony.phoneType
        val phoneCount = telephony.phoneCount

        println("Buzz Telephony phoneType: $phoneType, count: $phoneCount")

        // Get allCellInfo
        if (ActivityCompat.checkSelfPermission(
                activity,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                activity,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION
                ), Constants.LOCATION_PERMISSIONS_REQUEST_CODE
            )
        } else {
            val cellMap = HashMap<Int, JSONObject>()

            telephony.allCellInfo.forEach { info ->
                try {
                    val cellObj = JSONObject()

                    when (info) {
                        is CellInfoGsm -> {
                            val gsm = info.cellSignalStrength
                            val identityGsm = info.cellIdentity

                            cellObj.put("name", "GSM")
                            cellObj.put("cellId", identityGsm.cid)
                            cellObj.put("mcc", identityGsm.mccString)
                            cellObj.put("mnc", identityGsm.mncString)
                            cellObj.put("lac", identityGsm.lac)
                            cellObj.put("dbm", gsm.dbm)
                            cellObj.put("level", gsm.level)
                            cellObj.put("asuLevel", gsm.asuLevel)

                            cellMap[identityGsm.cid] = cellObj
                        }
                        is CellInfoLte -> {
                            val lte = info.cellSignalStrength
                            val identityLte = info.cellIdentity

                            cellObj.put("name", "LTE")
                            cellObj.put("operator", identityLte.mobileNetworkOperator)
                            cellObj.put("operatorAlphaLong", identityLte.operatorAlphaLong)
                            cellObj.put("operatorAlphaShort", identityLte.operatorAlphaShort)
                            cellObj.put("pci", identityLte.pci)
                            cellObj.put("describeContents", identityLte.describeContents())
                            cellObj.put("cellId", identityLte.ci)
                            cellObj.put("tac", identityLte.tac)
                            cellObj.put("bandwidth", identityLte.bandwidth)
                            cellObj.put("earfcn", identityLte.earfcn)
                            cellObj.put("mccString", identityLte.mccString)
                            cellObj.put("dbm", lte.dbm)
                            cellObj.put("asuLevel", lte.asuLevel)
                            cellObj.put("cqi", lte.cqi)
                            cellObj.put("rsrp", lte.rsrp)
                            cellObj.put("rsrq", lte.rsrq)
                            cellObj.put("rssi", lte.rssi)
                            cellObj.put("rssnr", lte.rssnr)
                            cellObj.put("timingAdvance", lte.timingAdvance)

                            cellMap[identityLte.ci] = cellObj
                        }
                        is CellInfoCdma -> {
                            val cdma = info.cellSignalStrength
                            val identityCdma = info.cellIdentity

                            cellObj.put("name", "CDMA")
                            cellObj.put("cellId", identityCdma.networkId)
                            cellObj.put("baseStationId", identityCdma.basestationId)
                            cellObj.put("networkId", identityCdma.networkId)
                            cellObj.put("latitude", identityCdma.latitude)
                            cellObj.put("longitude", identityCdma.longitude)
                            cellObj.put("systemId", identityCdma.systemId)
                            cellObj.put("dbm", cdma.cdmaDbm)
                            cellObj.put("level", cdma.level)
                            cellObj.put("asuLevel", cdma.asuLevel)

                            cellMap[identityCdma.networkId] = cellObj
                        }
                        is CellInfoNr -> {
                            val nr = info.cellSignalStrength
                            val identityNr = info.cellIdentity

                            cellObj.put("name", "5G NR")
                            cellObj.put("describeContents", identityNr.describeContents())
                            cellObj.put("operatorAlphaLong", identityNr.operatorAlphaLong)
                            cellObj.put("operatorAlphaShort", identityNr.operatorAlphaShort)
                            cellObj.put("dbm", nr.dbm)
                            cellObj.put("level", nr.level)
                            cellObj.put("asuLevel", nr.asuLevel)

                            cellMap[identityNr.describeContents()] = cellObj
                        }
                        is CellInfoWcdma -> {
                            val wcdma = info.cellSignalStrength
                            val identityWcdma = info.cellIdentity

                            cellObj.put("name", "WCDMA")
                            cellObj.put("cellId", identityWcdma.cid)
                            cellObj.put("lac", identityWcdma.lac)
                            cellObj.put("dbm", wcdma.dbm)
                            cellObj.put("level", wcdma.level)
                            cellObj.put("asuLevel", wcdma.asuLevel)

                            cellMap[identityWcdma.cid] = cellObj
                        }
                        is CellInfoTdscdma -> {
                            val scdma = info.cellSignalStrength
                            val identityScdma = info.cellIdentity

                            cellObj.put("name", "TD-SCDMA")
                            cellObj.put("cellId", identityScdma.cid)
                            cellObj.put("cellParamtersId", identityScdma.cpid)
                            cellObj.put("lac", identityScdma.lac)
                            cellObj.put("channelNumber", identityScdma.uarfcn)
                            cellObj.put("level", scdma.level)
                            cellObj.put("asuLevel", scdma.asuLevel)
                            cellObj.put("rscp", scdma.rscp)

                            cellMap[identityScdma.cid] = cellObj
                        }
                    }

                    fetchCellInfoResponse.postValue(cellMap)
                } catch (ex: Exception) {
                    println("cell info exception: ${ex.message}")
                }
            }
        }
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    fun fetchWifiInfo(activity: Activity) {
        val wifi = activity.getSystemService(WIFI_SERVICE) as WifiManager
        val wifiInfo = wifi.connectionInfo
        val wifiObj = JSONObject()

        wifiObj.put("ssid", wifiInfo.ssid)
        wifiObj.put("bssid", wifiInfo.bssid)
        wifiObj.put("frequency", wifiInfo.frequency)
        wifiObj.put("hiddenSSID", wifiInfo.hiddenSSID)
        wifiObj.put("ipAddress", wifiInfo.ipAddress)
        wifiObj.put("linkSpeed", wifiInfo.linkSpeed)
        wifiObj.put("macAddress", wifiInfo.macAddress)
        wifiObj.put("networkId", wifiInfo.networkId)
        wifiObj.put("rssi", wifiInfo.rssi)
        wifiObj.put("supplicantState", wifiInfo.supplicantState)
        wifiObj.put("txLinkSpeedMbps", wifiInfo.txLinkSpeedMbps)

        val data = HashMap<String, JSONObject>()
        data[wifiInfo.ssid] = wifiObj

        fetchWifiInfoResponse.postValue(data)

    }
}