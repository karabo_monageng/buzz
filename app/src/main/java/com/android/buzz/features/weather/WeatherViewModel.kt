package com.android.buzz.features.weather

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import okhttp3.*
import java.io.IOException
import javax.inject.Inject

class WeatherViewModel @Inject constructor() : ViewModel() {
    private val okHttpClient = OkHttpClient()

    val getCurrentWeatherResponse = MutableLiveData<WeatherResponseModel>()
    val isLoadingWeather = MutableLiveData<Pair<Boolean, String?>>()

    fun getCurrentWeather(latitude: Double, longitude: Double) {
        isLoadingWeather.postValue(Pair(true, null))
        val request = Request.Builder()
            .url("https://api.openweathermap.org/data/2.5/weather?lat=$latitude&lon=$longitude&appid=3c735be352f428003f6f6c7130740ead&units=metric")
            .build()
        okHttpClient.newCall(request)
            .enqueue(object : Callback {
                override fun onResponse(call: Call, response: Response) {
                    isLoadingWeather.postValue(Pair(false, null))
                    if(response.isSuccessful) {
                        val result = response.body?.string()!!
                        println("OkHttpResponse $result}")

                        val moshi = Moshi.Builder().build()
                        val adapter: JsonAdapter<WeatherResponseModel> = moshi.adapter(WeatherResponseModel::class.java)
                        val payload = adapter.fromJson(result)

                        println("OkHttpResponse, current weather: ${payload?.weather?.first()?.description}/${payload?.main?.temp}")

                        getCurrentWeatherResponse.postValue(payload)
                    } else {
                        println("OkHttpResponse failed")
                        getCurrentWeatherResponse.postValue(null)
                    }
                }
                override fun onFailure(call: Call, e: IOException) {
                    isLoadingWeather.postValue(Pair(false, e.message))
                    println("OkHttpResponse failed: ${e.message}")
                }
            })
    }
}