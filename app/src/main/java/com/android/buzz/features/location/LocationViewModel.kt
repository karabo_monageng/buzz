package com.android.buzz.features.location

import android.Manifest
import android.app.Activity
import android.app.Application
import android.content.Context.LOCATION_SERVICE
import android.content.pm.PackageManager
import android.location.Location
import android.location.LocationListener
import android.location.LocationManager
import android.os.Bundle
import androidx.core.app.ActivityCompat
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.android.buzz.database.BuzzDatabase
import com.android.buzz.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import javax.inject.Inject

class LocationViewModel @Inject constructor(application: Application) : ViewModel() {
    private val locationDao = BuzzDatabase.getDatabase(application)?.locationDao()

    val isLoadingLocation = MutableLiveData<Pair<Boolean, String?>>()
    val currentLocationResponse = MutableLiveData<Location>()
    val pinUserLocationResponse = MutableLiveData<Pair<Boolean, String?>>()
    val savedLocationsResponse = MutableLiveData<Pair<Boolean, List<UserLocationModel>>>()

    fun updateUserLocation(userLocation: UserLocationModel) {
        GlobalScope.launch(Dispatchers.IO) {
            runCatching {
                locationDao?.updateLocation(userLocation)
            }.onSuccess {
                pinUserLocationResponse.postValue(Pair(true, null))
            }.onFailure {
                pinUserLocationResponse.postValue(Pair(false, it.message))
            }
        }
    }

    fun pinUserLocation(userLocation: UserLocationModel) {
        GlobalScope.launch(Dispatchers.IO) {
            runCatching {
                locationDao?.insertLocation(userLocation)
            }.onSuccess {
                pinUserLocationResponse.postValue(Pair(true, null))
            }.onFailure {
                pinUserLocationResponse.postValue(Pair(false, it.message))
            }
        }
    }

    fun getSavedLocations() {
        GlobalScope.launch(Dispatchers.IO) {
            runCatching {
                locationDao?.getLocationList()
            }.onSuccess { savedLocations ->
                savedLocationsResponse.postValue(Pair(true, savedLocations!!))
            }.onFailure {
                savedLocationsResponse.postValue(Pair(false, listOf()))
            }
        }
    }

    fun getCurrentLocation(activity: Activity) {
        isLoadingLocation.postValue(Pair(true, null))

        val locationManager = activity.applicationContext.getSystemService(LOCATION_SERVICE) as LocationManager

        val locationListener = object : LocationListener {
            override fun onLocationChanged(location: Location?) {
                isLoadingLocation.postValue(Pair(false, null))
                currentLocationResponse.postValue(location)
            }

            override fun onProviderDisabled(provider: String?) {

            }

            override fun onProviderEnabled(provider: String?) {

            }

            override fun onStatusChanged(provider: String?, status: Int, extras: Bundle?) {

            }
        }

        try {
            if (ActivityCompat.checkSelfPermission(
                    activity.applicationContext,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                    activity.applicationContext,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // If permissions are not set, request them
                ActivityCompat.requestPermissions(activity,
                    arrayOf(
                        Manifest.permission.ACCESS_FINE_LOCATION,
                        Manifest.permission.ACCESS_COARSE_LOCATION
                    ), Constants.LOCATION_PERMISSIONS_REQUEST_CODE
                )
            } else {
                // Otherwise request location updates
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000L, 10f, locationListener)
            }

        } catch (ex: Exception) {
            isLoadingLocation.postValue(Pair(false, ex.message))
        }
    }
}