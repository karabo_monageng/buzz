package com.android.buzz.features.location

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.squareup.moshi.JsonClass

@Entity(tableName = "UserLocation")
@JsonClass(generateAdapter = true)
data class UserLocationModel (
    @PrimaryKey(autoGenerate = true)
    var id: Int? = 0,

    var latitude: Double = 0.0,
    var longitude: Double = 0.0,
    var altitude: Double? = null,
    var name: String = "",
    var description: String? = "",
    var dateTime: String = ""
)