package com.android.buzz.ui.home.fragments.locationdetails

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.buzz.R
import com.android.buzz.databinding.FragmentLocationDetailsBinding
import com.android.buzz.features.location.LocationViewModel
import com.android.buzz.features.location.UserLocationModel
import com.squareup.moshi.Moshi
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_location_details.view.*
import javax.inject.Inject

@AndroidEntryPoint
class LocationDetailsFragment : Fragment() {
    private lateinit var detailsBinding: FragmentLocationDetailsBinding
    @Inject
    lateinit var locationViewModel: LocationViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_location_details, container, false)

        detailsBinding = FragmentLocationDetailsBinding.bind(view)

        return detailsBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val bundle: String = arguments?.getString("UserLocationModel")!!
        val moshi = Moshi.Builder().build()
        val adapter = moshi.adapter(UserLocationModel::class.java)
        val userLocationModel = adapter.fromJson(bundle)

        detailsBinding.userLocation = userLocationModel

        locationViewModel.pinUserLocationResponse.observe(viewLifecycleOwner, { response ->
            if(response.first) {
                AlertDialog.Builder(requireContext())
                    .setTitle(getString(R.string.success))
                    .setMessage(getString(R.string.location_updated_success))
                    .setPositiveButton(getString(R.string.done)) { dialog, which ->
                        dialog.dismiss()
                        findNavController().popBackStack()
                    }
                    .create()
                    .show()
            } else {
                Log.e("PinLocationFragment", response?.second!!)

                AlertDialog.Builder(requireContext())
                    .setTitle(getString(R.string.error))
                    .setMessage(response.second)
                    .setPositiveButton(getString(R.string.ok)) { dialog, which ->
                        dialog.dismiss()
                    }
                    .create()
                    .show()
            }
        })
        view.button_update.setOnClickListener {
            // Update values before saving them
            userLocationModel?.name = view.text_location_name.editText?.text.toString()
            userLocationModel?.description = view.text_location_description.editText?.text.toString()

            locationViewModel.updateUserLocation(userLocationModel!!)
        }

        view.text_location_name.editText?.doOnTextChanged { text, start, before, count ->
            if(text?.isEmpty()!!) {
                view.button_update.visibility = View.GONE
            } else {
                view.button_update.visibility = View.VISIBLE
            }
        }

        view.text_location_description.editText?.doOnTextChanged { text, start, before, count ->
            if(text?.isEmpty()!!) {
                view.button_update.visibility = View.GONE
            } else {
                view.button_update.visibility = View.VISIBLE
            }
        }
    }
}