package com.android.buzz.ui.home.fragments.locations

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.android.buzz.databinding.ItemListLocationBinding
import com.android.buzz.features.location.UserLocationModel

class LocationsAdapter(private val savedLocations: List<UserLocationModel>) : RecyclerView.Adapter<LocationsAdapter.LocationsViewHolder>() {
    var onItemClick: ((UserLocationModel) -> Unit)? = null

    inner class LocationsViewHolder(private val binding: ItemListLocationBinding) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                onItemClick?.invoke(binding.userLocation!!)
            }
        }

        fun bind(userLocationModel: UserLocationModel) {
            binding.userLocation = userLocationModel
            binding.executePendingBindings()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationsViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemListLocationBinding.inflate(inflater, parent, false)

        return LocationsViewHolder(binding)
    }

    override fun onBindViewHolder(holder: LocationsViewHolder, position: Int) {
        holder.bind(savedLocations[position])
    }

    override fun getItemCount(): Int = savedLocations.size
}