package com.android.buzz.ui.home.fragments.networkinfo

import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import com.android.buzz.R
import com.android.buzz.features.network.NetworkViewModel
import com.google.android.material.chip.Chip
import com.google.android.material.chip.ChipGroup
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_notifications.view.*
import java.util.*
import javax.inject.Inject

@AndroidEntryPoint
class NetworkInfoFragment : Fragment() {
    private val TAG = javaClass.simpleName

    @Inject lateinit var networkViewModel: NetworkViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_notifications, container, false)
    }

    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Phone Network Info
        networkViewModel.fetchCellInfoResponse.observe(viewLifecycleOwner, { networkData ->
            networkData.forEach { data ->
                println("$TAG Single network info: ${data.value}")
                // Generate ChipGroup colors randomly
                val newRandom = Random()
                val randomGroupColor: Int = Color.argb(255, newRandom.nextInt(256), newRandom.nextInt(256), newRandom.nextInt(256))
                val chipGroup = ChipGroup(requireContext()).apply {
                    this.setBackgroundColor(randomGroupColor)
                }

                chipGroup.addView(Chip(requireContext()).apply {
                    text = "NetworkID: ${data.key}"
                })
                if (data.value.has("name")) chipGroup.addView(Chip(requireContext()).apply {
                    text = "Name: ${data.value.getString("name")}"
                })
                if (data.value.has("operator")) chipGroup.addView(Chip(requireContext()).apply {
                    text = "TowerID: ${data.value.getInt("operator")}"
                })
                if (data.value.has("operatorAlphaLong")) chipGroup.addView(Chip(requireContext()).apply {
                    val payload = data.value.getString("operatorAlphaLong")
                    text = "Operator: $payload"
                    if(payload.isNullOrEmpty()) {
                        this.visibility = View.GONE
                    } else {
                        this.visibility = View.VISIBLE
                    }
                })
                if (data.value.has("pci")) chipGroup.addView(Chip(requireContext()).apply {
                    text = "pci: ${data.value.getInt("pci")}"
                })
                if (data.value.has("operator")) chipGroup.addView(Chip(requireContext()).apply {
                    text = "CellID: ${data.value.getInt("cellId")}"
                })
                if (data.value.has("tac")) chipGroup.addView(Chip(requireContext()).apply {
                    text = "tac: ${data.value.getInt("tac")}"
                })
                if (data.value.has("bandwidth")) chipGroup.addView(Chip(requireContext()).apply {
                    text = "bandwidth: ${data.value.getInt("bandwidth")}"
                })
                if (data.value.has("earfcn")) chipGroup.addView(Chip(requireContext()).apply {
                    text = "earfcn: ${data.value.getInt("earfcn")}"
                })
                if (data.value.has("mccString")) chipGroup.addView(Chip(requireContext()).apply {
                    val payload = data.value.getString("mccString")
                    text = "mccString: $payload"
                    if(payload.isNullOrEmpty()) {
                        this.visibility = View.GONE
                    } else {
                        this.visibility = View.VISIBLE
                    }
                })
                if (data.value.has("dbm")) chipGroup.addView(Chip(requireContext()).apply {
                    text = "dbm: ${data.value.getInt("dbm")}"
                })
                if (data.value.has("asuLevel")) chipGroup.addView(Chip(requireContext()).apply {
                    text = "asuLevel: ${data.value.getInt("asuLevel")}"
                })
                if (data.value.has("rssi")) chipGroup.addView(Chip(requireContext()).apply {
                    text = "rssi: ${data.value.getInt("rssi")}"
                })

                view.linear_cell_container.addView(
                    chipGroup,
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                )

            }
        })
        networkViewModel.fetchCellInfo(requireActivity())

        // WiFi Info
        networkViewModel.fetchWifiInfoResponse.observe(viewLifecycleOwner, { wifiData ->
            wifiData.forEach { data ->
                println("$TAG WiFi network info: ${data.value}")

                val newRandom = Random()
                val randomGroupColor: Int = Color.argb(255, newRandom.nextInt(256), newRandom.nextInt(256), newRandom.nextInt(256))
                val chipGroup = ChipGroup(requireContext()).apply {
                    this.setBackgroundColor(randomGroupColor)
                }

                chipGroup.addView(Chip(requireContext()).apply {
                    text = "WiFi SSID: ${data.value.getString("ssid")}"
                })
                chipGroup.addView(Chip(requireContext()).apply {
                    text = "BSSID: ${data.value.getString("bssid")}"
                })
                chipGroup.addView(Chip(requireContext()).apply {
                    text = "Frequency: ${data.value.getInt("frequency")} Hz"
                })
                chipGroup.addView(Chip(requireContext()).apply {
                    text = "Link Speed: ${data.value.getInt("linkSpeed")}"
                })
                chipGroup.addView(Chip(requireContext()).apply {
                    text = "ID: ${data.value.getInt("networkId")}"
                })
                chipGroup.addView(Chip(requireContext()).apply {
                    text = "RSSI: ${data.value.getInt("rssi")}"
                })

                view.linear_cell_container.addView(
                    chipGroup,
                    LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                    )
                )
            }
        })
        networkViewModel.fetchWifiInfo(requireActivity())

    }
}