package com.android.buzz.ui.home.fragments.pinlocation

import android.app.AlertDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.buzz.R
import com.android.buzz.features.location.LocationViewModel
import com.android.buzz.features.location.UserLocationModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_pin_location.view.*
import java.time.LocalDateTime
import javax.inject.Inject

@AndroidEntryPoint
class PinLocationFragment : Fragment() {
    @Inject
    lateinit var locationViewModel: LocationViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_pin_location, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val latitude = arguments?.getDouble(getString(R.string.latitude))
        val longitude = arguments?.getDouble(getString(R.string.longitude))
        val altitude = arguments?.getDouble(getString(R.string.altitude))

        view.text_pin_title.text = getString(R.string.location_pin_summary, latitude, longitude, altitude)

        locationViewModel.pinUserLocationResponse.observe(viewLifecycleOwner, { response ->
            if(response.first) {
                AlertDialog.Builder(requireContext())
                    .setTitle(getString(R.string.success))
                    .setMessage(getString(R.string.location_saved_success))
                    .setPositiveButton(getString(R.string.done)) { dialog, which ->
                        dialog.dismiss()
                        findNavController().popBackStack()
                    }
                    .create()
                    .show()
            } else {
                Log.e("PinLocationFragment", response?.second!!)

                AlertDialog.Builder(requireContext())
                    .setTitle(getString(R.string.error))
                    .setMessage(response.second)
                    .setPositiveButton(getString(R.string.ok)) { dialog, which ->
                        dialog.dismiss()
                    }
                    .create()
                    .show()
            }
        })

        view.fab_save_location.setOnClickListener {
            val user = validateInput(view, latitude!!, longitude!!, altitude!!)

            if(user != null) {
                locationViewModel.pinUserLocation(user)
            }
        }
    }

    private fun validateInput(view: View, latitude: Double, longitude: Double, altitude: Double) : UserLocationModel? {
        view.text_field_name.editText?.apply {
            when {
                this.text.toString().isEmpty() -> {
                    this.error = context.getString(R.string.enter_valid_name)
                }
                else -> {
                    return UserLocationModel(
                        name = this.text.toString(),
                        description = view.text_field_description.editText?.text.toString(),
                        dateTime = LocalDateTime.now().toString(),
                        latitude = latitude,
                        longitude = longitude,
                        altitude = altitude
                    )
                }
            }
        }

        return null
    }
}