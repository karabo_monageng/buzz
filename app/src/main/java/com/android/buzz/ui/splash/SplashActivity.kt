package com.android.buzz.ui.splash

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import com.android.buzz.R
import com.android.buzz.ui.home.HomeActivity

class SplashActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        supportActionBar?.hide()

        Handler().postDelayed({
            Intent(this@SplashActivity, HomeActivity::class.java).apply {
                startActivity(this)
                finish()
            }
        }, 4000)
    }
}