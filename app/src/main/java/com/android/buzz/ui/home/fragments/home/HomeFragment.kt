package com.android.buzz.ui.home.fragments.home

import android.app.AlertDialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.android.buzz.R
import com.android.buzz.features.location.LocationViewModel
import com.android.buzz.features.weather.WeatherViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_home.view.*
import javax.inject.Inject

@AndroidEntryPoint
class HomeFragment : Fragment() {
    private val TAG = javaClass.simpleName
    private lateinit var coordinates: Triple<Double, Double, Double>

    @Inject lateinit var locationViewModel: LocationViewModel
    @Inject lateinit var weatherViewModel: WeatherViewModel


    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // Weather Info
        weatherViewModel.getCurrentWeatherResponse.observe(viewLifecycleOwner, { weatherModel ->
            view.text_weather_update.text = getString(
                R.string.current_weather,
                weatherModel.name,
                weatherModel.main?.temp,
                weatherModel.main?.feelsLike,
                weatherModel.main?.humidity,
                weatherModel.weather?.first()?.main,
                weatherModel.weather?.first()?.description
            )
        })

        // Weather Info Animation
        weatherViewModel.isLoadingWeather.observe(viewLifecycleOwner, { isLoading ->
            if(isLoading.first) {
                view.lottie_loader_weather.visibility = View.VISIBLE
            } else {
                view.lottie_loader_weather.visibility = View.GONE
            }
            if(!isLoading?.second.isNullOrEmpty()) {
                view.card_weather_details.visibility = View.GONE
                AlertDialog.Builder(requireContext())
                    .setTitle("Ooops..")
                    .setMessage("Looks like there's no Internet connection to get Weather info")
                    .setCancelable(true)
                    .setPositiveButton(
                        "Retry"
                    ) { dialog, which ->
                        weatherViewModel.getCurrentWeather(coordinates.first, coordinates.second)
                        dialog.dismiss()
                    }.create()
                    .show()
            }
        })

        // Location Info
        locationViewModel.currentLocationResponse.observe(viewLifecycleOwner, { location ->
            coordinates = Triple(location.latitude, location.longitude, location.altitude)

            println("$TAG Current location details, lat/lon: ${location.latitude}/${location.longitude}, altitude: ${location.altitude} ")
            view.text_home.text = getString(R.string.location_data, location.latitude, location.longitude, location.altitude, location.accuracy)

            view.fab_pin_location.visibility = View.VISIBLE
            view.card_weather_details.visibility = View.VISIBLE

            // Get Weather info based on location
            weatherViewModel.getCurrentWeather(location.latitude, location.longitude)

        })

        // Location Info Animation
        locationViewModel.isLoadingLocation.observe(viewLifecycleOwner, { isLoading ->
            if(isLoading.first) {
                view.lottie_loader_map.visibility = View.VISIBLE
            } else {
                view.lottie_loader_map.visibility = View.GONE
            }
        })

        locationViewModel.getCurrentLocation(requireActivity())

        view.fab_pin_location.setOnClickListener {
            val bundle = bundleOf(
                getString(R.string.latitude) to coordinates.first,
                getString(R.string.longitude) to coordinates.second,
                getString(R.string.altitude) to coordinates.third
            )

            findNavController().navigate(R.id.action_home_pin_location, bundle)
        }

    }

}