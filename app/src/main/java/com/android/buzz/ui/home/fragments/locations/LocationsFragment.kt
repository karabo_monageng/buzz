package com.android.buzz.ui.home.fragments.locations

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.buzz.R
import com.android.buzz.features.location.LocationViewModel
import com.android.buzz.features.location.UserLocationModel
import com.squareup.moshi.JsonAdapter
import com.squareup.moshi.Moshi
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.fragment_locations.view.*
import javax.inject.Inject

@AndroidEntryPoint
class LocationsFragment : Fragment() {

    @Inject lateinit var locationViewModel: LocationViewModel

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_locations, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        locationViewModel.savedLocationsResponse.observe(viewLifecycleOwner, { response ->
            if(response.first) {
                view.text_no_locations.visibility = View.GONE
                val savedLocations = response.second

                println("Saved user locations count: ${savedLocations.size}")
                if(savedLocations.isNotEmpty()) {
                    view.text_no_locations.visibility = View.GONE

                    view.recycler_saved_locations.layoutManager = LinearLayoutManager(
                        requireContext(),
                        LinearLayoutManager.VERTICAL,
                        false)

                    val locationsAdapter = LocationsAdapter(savedLocations)
                    view.recycler_saved_locations.adapter = locationsAdapter

                    locationsAdapter.onItemClick = { locationModel ->
                        val moshi = Moshi.Builder().build()
                        val adapter: JsonAdapter<UserLocationModel> = moshi.adapter(UserLocationModel::class.java)
                        val payload: String = adapter.toJson(locationModel)
                        val bundle: Bundle? = bundleOf("UserLocationModel" to payload)

                        findNavController().navigate(R.id.action_saved_locations_view_details, bundle)
                    }
                }

            } else {
                view.text_no_locations.visibility = View.VISIBLE
                println("There are no locations saved locally")
            }
        })

        locationViewModel.getSavedLocations()
    }
}