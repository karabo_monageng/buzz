package com.android.buzz.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.migration.Migration
import androidx.sqlite.db.SupportSQLiteDatabase
import com.android.buzz.features.location.LocationDao
import com.android.buzz.features.location.UserLocationModel


@Database(entities = [UserLocationModel::class], version = 2, exportSchema = false)
abstract class BuzzDatabase : RoomDatabase() {
    abstract fun locationDao() : LocationDao

    companion object {
        private var INSTANCE: BuzzDatabase? = null

        private val MIGRATION_1_2: Migration? = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("ALTER TABLE UserLocation ADD COLUMN altitude REAL")
            }
        }

        fun getDatabase(context: Context) : BuzzDatabase? {
            if(INSTANCE == null) {
                synchronized(BuzzDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        BuzzDatabase::class.java,
                        "BuzzDatabase"
                    )
                        .addMigrations(MIGRATION_1_2)
                        .build()
                }
            }
            return INSTANCE
        }
    }
}